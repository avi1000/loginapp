const express = require('express');
const fs = require('fs');
const app = express();
const router = express.Router();
let bodyParser = require('body-parser');
const PORT = 3333;

const allowCrossDomain = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', "*");
    next();
}

var server = app.listen(PORT, '0.0.0.0', () => {
    console.log(`server starting on PORT ${PORT}`);
});
router.get('/forgot-password', (req, res) => {
    //TODO: a check for the email to see if it exists in the system
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.send(JSON.stringify({ statusCode: 200 }));
    writeToLogFile(`email was sent at: ${getWrittenTime()}\r\n`);
});

router.get('/forgot-password', (req, res) => {
    //TODO: a check for the email to see if it exists in the system
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.send(JSON.stringify({ statusCode: 200 }));
    writeToLogFile(`email was sent at: ${getWrittenTime()}\r\n`);
});

router.get('/logout', (req, res) => {
    //TODO: a check for the email to see if it exists in the system
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.send(JSON.stringify({ statusCode: 200 }));
    writeToLogFile(`logout at: ${getWrittenTime()}\r\n`);
});

router.get('/login', (req, res) => {
    //TODO: a check for the email to see if it exists in the system
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.send(JSON.stringify({ statusCode: 200 }));
    writeToLogFile(`login at: ${getWrittenTime()}\r\n`);
});

function writeToLogFile(logMessage){
    fs.appendFile('log.txt', logMessage, (err)=> {
        if (err) throw err;
            console.log('file was written!');
      });    
}   
function getWrittenTime(){
    const dateTime = require('node-datetime');
    let dt = dateTime.create();
    return dt.format('Y-m-d H:M:S');
}
app.use(router);
module.exports.app = app;