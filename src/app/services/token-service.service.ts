import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable()
export class TokenService {
  private TOKEN_AUTH_KEY: string = 'tk';
  private SESSION_TIMEOUT: number = 10000;
  constructor() {

  }
  public validateStorageToken() {
    const tokenState = JSON.parse(localStorage.getItem(this.TOKEN_AUTH_KEY));
    return tokenState.validated == 'true';
  }
  public setStorageToken(value: Object) {
    localStorage.setItem(this.TOKEN_AUTH_KEY, JSON.stringify(value));
  }
  public resetToken(forceReset: boolean = false) {
    const token = JSON.parse(localStorage.getItem(this.TOKEN_AUTH_KEY));
    if (forceReset || (!token.isPersistant && this.isSessionTimeElapsed()))
      this.setStorageToken({ validated: 'false' });
    }

  public refreshToken() {
    let token = JSON.parse(localStorage.getItem(this.TOKEN_AUTH_KEY));
    if (token && !token.isPersistant && this.isSessionTimeElapsed()) {
      this.resetToken();
      firebase.auth().onAuthStateChanged((user) => {
        if (user && user.email) {
          if (!token.isPersistant) {
            token.timestamp = parseFloat(token.timestamp) + this.SESSION_TIMEOUT;
          }
        }
        else
          token = { validated: 'false' };
      });
      this.setStorageToken(token);
    }
  }
  public isTokenPersistant() {
    let token = JSON.parse(localStorage.getItem(this.TOKEN_AUTH_KEY));
    return token.isPersistant;
  }
  private isSessionTimeElapsed() {
    let token = JSON.parse(localStorage.getItem(this.TOKEN_AUTH_KEY));
    return token.timestamp + this.SESSION_TIMEOUT <= new Date().getTime();
  }
}
