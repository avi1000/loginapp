import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { RequestOptions } from '@angular/http/src/base_request_options';
import * as firebase from 'firebase';
import { debug, error } from 'util';
import { IUser } from '../models/user';
import { IUserCredentials } from '../models/user-credentials';
import { IForgotPasswordResponse } from '../models/forgot-password-response';
import { TokenService } from './token-service.service';

enum SignInOptions {
    EmailPassword = 1,
    Facebook = 2,
    Twitter = 3,
    Google = 4
}
@Injectable()
export class AuthService {

    private LOGOUT_URL: string = '/login';
    private SERVER_FORGOT_PASSWORD_URL: string = 'http://localhost:3333/forgot-password';
    private SERVER_LOGIN_URL: string = 'http://localhost:3333/login';
    private SERVER_LOGOUT_URL: string = 'http://localhost:3333/logout';


    constructor(private http: Http,
        private router: Router, private tokenService: TokenService) {

    }

    public login(userCredentials: IUserCredentials) {

        return new Promise((resolve, reject) => {
            this.setPersistanceState(userCredentials)
                .then(() => {
                    firebase.auth().signInWithEmailAndPassword(userCredentials.email, userCredentials.password)
                        .then((responseToken: any) => {
                            const storageTokenObject = this.createStorageToken(userCredentials);
                            this.tokenService.setStorageToken(storageTokenObject);
                            this.http.get(this.SERVER_LOGIN_URL, {}).subscribe(()=>{
                            resolve(true);
                            });
                        })
                        .catch((error) => {
                            this.tokenService.setStorageToken({ validated: 'false' });
                            resolve(false);
                        });
                });
        })
            .catch((error) => {
                console.log('login has failed');
            });
    }

    public logout() {
        return firebase.auth().signOut()
            .then(() => {
                this.tokenService.setStorageToken({ validated: 'false' });
                this.http.get(this.SERVER_LOGOUT_URL, {}).subscribe(()=>{
                    this.router.navigate([this.LOGOUT_URL]);
                });
            }).catch((error) => {
                //TODO: handle error
                console.log('logout has failed');
            });
    }

    public sendForgotPasswordEmail(email: string) {
        return this.http.get(this.SERVER_FORGOT_PASSWORD_URL, {});
    }

    public isUserAuthenticated() {
        return this.tokenService.validateStorageToken();
    }

    public getLoggedInUser(): Promise<string> {
        return new Promise((resolve, reject) => {
            firebase.auth().onAuthStateChanged((user) => {
                if (user) {
                    debugger;
                    resolve(user.email);
                }
                else
                    resolve(null);
            });
        });
    }

    public refreshState() {
        this.tokenService.refreshToken();
    }
    private getPersistanceOption(userCredentials: IUserCredentials): any {
        return userCredentials.persistentLoggedIn ?
            firebase.auth.Auth.Persistence.LOCAL :
            firebase.auth.Auth.Persistence.SESSION;
    }

    private setPersistanceState(userCredentials: IUserCredentials) {
        const persistanceOption = this.getPersistanceOption(userCredentials);
        return firebase.auth().setPersistence(persistanceOption);
    }
    private createStorageToken(userCredentials: IUserCredentials) {
        return {
            validated: 'true',
            email: userCredentials.email,
            isPersistant: userCredentials.persistentLoggedIn,
            timestamp: userCredentials.persistentLoggedIn ? -1 : new Date().getTime()
        }
    }
}
