import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { TokenService } from './services/token-service.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  constructor(private router: Router,
    private authService: AuthService,
    private tokenService: TokenService) {

  }
  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyBEvgbgXm8QEUixd_AtwE8a70OXENYkjhU",
      authDomain: "loginapp-e2500.firebaseapp.com",
    });
    this.authService.refreshState();
  }
  onWindowRefreshed(e) {
    localStorage.setItem('w', e.srcElement.constructor.name);
    let isClosed = this.isWindowClosed(e);
    if (isClosed && !this.tokenService.isTokenPersistant())
      this.tokenService.resetToken(true);
  }
  private isWindowClosed(e) {
    e = e || window.event;
    const y = e.pageY || e.clientY;
    return y < 0;
  }
}
