import { Component, OnInit } from '@angular/core';
import { IUser } from '../../models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.less']
})
export class WelcomeComponent implements OnInit {
  // public readonly currentUser: User;
  email: string = '';


  constructor(private authenticationService: AuthService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.authenticationService.getLoggedInUser()
      .then((email) => {
        if(email)
          this.email = email;
          else
          this.router.navigate(['/login']);
      });
  }

  logout() {
    this.authenticationService.logout();
  }

}
