import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Response } from '@angular/http/src/static_response';
import { IForgotPasswordResponse } from '../../models/forgot-password-response';
import { debug } from 'util';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { NgForm } from '@angular/forms/src/directives/ng_form';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.less']
})
export class ForgotPasswordComponent implements OnInit {
  isEmailSent: boolean = false;
  constructor(private authenticationService: AuthService) {

  }

  ngOnInit() {
  }

  resetPassword(email, form:NgForm) {
    this.authenticationService.sendForgotPasswordEmail(email)
      .subscribe((res: Response) => {
        const data = <IForgotPasswordResponse>res.json();
        this.validateResetResponse(data);
      },
      (error) => {
      });
  }

  private validateResetResponse(serverResponse:IForgotPasswordResponse){
      this.isEmailSent = serverResponse.statusCode == 200;
  }
}
