import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { IUserCredentials } from '../../models/user-credentials';
import { NgForm } from '@angular/forms/src/directives/ng_form';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  model: IUserCredentials = {
    email: '',
    password: '',
    persistentLoggedIn: false
  };
  private REDIRECT_URL = '/welcome';
  isLoginFailed: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService) { 

      }

  ngOnInit() {

  }
  login() {
    this.authenticationService.login(this.model)
      .then((result) => {
        if (result)
          this.router.navigate([this.REDIRECT_URL]);
        else
          this.isLoginFailed = true;
      });
  }
}
