export interface IUserCredentials {
    email:string;
    password:string;
    persistentLoggedIn:boolean;
}
