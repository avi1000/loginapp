1. The app consists of a client and dummy server written in node.
2. The server is there only to simulate http requests like was asked in the test.
•	The solution should contain at least 2 http requests (this can be to a local Json file or any webservice they want to use)
3. I used firebase for the authentication.
4. The log file is log.txt inside server directory.

Steps to run the app: 
1. run npm i inside the root app. 
2. run npm i inside the server directory. 
3. inside the server directory, run npm app.js to start the server. 
4. run ng serve in the main root app to run the client app

General Notes: 
1. I created one email account to test the flow. i.e. login->welcome page (test1@gmail.com) 
2. Since the test requested to make at least 2 http calls AND to log them, I created a mock server in node to write the data to a file (log.txt).
3. The test said: 
• Remember Password should be implemented 
• Remember password option should be implemented 
I assume the 2nd one is forgot password. If so, I just made a call to the mock server to simulate an email which was sent.
4. There was no details as for the design, so I just made the pages as simple as possible. (with bootstrap 4)


Acceptance tests:
1. User logs in without "remember me" -> directed to welcome page
2. User close the browser/tab after logging in without "remember me" and tries to enter the welcome page -> directed to login page
3. User logs in with "remember me" -> directed to welcome page
4. User close the browser/tab after logging in with "remember me" and tries to enter the welcome page -> goes to welcome page
5. User logs out -> directed to login page
6. User tries to enter a url which doesn't exists -> directed to a "not found" page
7. User enters an invalid email -> an error message is displayed